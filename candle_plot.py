import pandas as pd
import utils
import plotly.graph_objects as go
import instrument


pair = 'EUR_USD'
granularity = 'H1'

df = pd.read_pickle(utils.get_his_data_filename(pair, granularity))

non_cols = ['time','volume']

mod_cols = [x for x in df.columns if x not in non_cols]

df[mod_cols] = df[mod_cols].apply(pd.to_numeric)

df_ma = df[['time','mid_o','mid_h','mid_l','mid_c']].copy()


df_plot = df.iloc[-100:]





fig = go.Figure()
fig.add_trace(go.Candlestick(
    x=df_plot.time,
    open = df_plot.mid_o,
    high = df_plot.mid_h,
    low = df_plot.mid_l,
    close = df_plot.mid_c,
    line=dict(width=1), opacity=1,
))

fig.update_layout(width=1000,
                  height=400,
                  paper_bgcolor='#1e1e1e',
                  plog_bgcolor='#1e1e1e',
                  margin=dict(l=10, r=10, b=10, t=10),
                  font = dict(size=10, color='#e1e1e1')
                  )

fig.update_xaxes(gridcolor='#1f292f',
                showgrid=True
                )

fig.update_yaxes(gridcolor='#1f292f',
                 showgrid=True)


#fig.show()

