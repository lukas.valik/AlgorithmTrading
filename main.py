#Raw pakages
import pandas as pd
import plotly.graph_objects as go
import yfinance as yf
import numpy as np

pd.set_option('display.max_columns', None)
pd.set_option('display.width', 320)

sdate = pd.to_datetime('2010-02-04')
edate = pd.to_datetime('2015-08-30')

#Nikkei
df = yf.download('^N225', sdate, edate)

index = pd.date_range(edate, periods=25, freq='D')
columns = df.columns

dfna = pd.DataFrame(index=index, columns=columns)
df = pd.concat([df, dfna])


# Ichimoku calculations
# 1. Tenkan-sen (Converstion Line): (9-period high + 9-period low) /2
period9_high = df['High'].rolling(window=9).max()
period9_low = df['Low'].rolling(window=9).min()
df['tenkan_sen'] = (period9_high+period9_low) / 2


# 2. Kijun-sen (Base Line): (26-period high + 26-period low) /2
period26_high = df['High'].rolling(window=26).max()
period26_low = df['Low'].rolling(window=26).min()
df['kijun_sen'] = (period26_high+period26_low) / 2


# 3. Senkou span A (Leading span A): (Conversion Line + Base Line) /2
df['senkou_span_a'] = ((df['tenkan_sen'] + df['kijun_sen']) / 2).shift(26)


# 4. Senkou Span B (Leading Span B): (52-period high + 52-period low) /2
period52_high = df['High'].rolling(window=52).max()
period52_low = df['Low'].rolling(window=52).min()
df['senkou_span_b'] = ((period52_high+period52_low) / 2).shift(26)


# 5. The most current closing price plotted 26 time periods behind (optional)
df['chikou_span'] = df['Close'].shift(-26)



fig = go.Figure()
fig2 = go.Figure()


fig.add_trace(go.Candlestick(x=df.index,
                             open=df['Open'],
                             high=df['High'],
                             low=df['Low'],
                             close=df['Close'],
                             name='market data'))

fig.add_trace(go.Scatter(x=df.index, y=df['tenkan_sen'], line=dict(color='royalblue', width=.7), name='Tenkane Sen'))
fig.add_trace(go.Scatter(x=df.index, y=df['kijun_sen'], line=dict(color='orange', width=.7), name='Kijun Sen'))
fig.add_trace(go.Scatter(x=df.index, y=df['senkou_span_a'], line=dict(color='black', width=.7), name='Senkou Span A'))
fig.add_trace(go.Scatter(x=df.index, y=df['senkou_span_b'], line=dict(color='purple', width=1.7), name='Senkou Span B', fill='tonexty', fillcolor='green', opacity=.01))
fig.add_trace(go.Scatter(x=df.index, y=df['chikou_span'], line=dict(color='red', width=.7), name='Chikou Span'))




df.dropna(inplace=True)


# Market data Above or Below the cloud
df['above_cloud'] = 0
df['above_cloud'] = np.where((df['Low'] > df['senkou_span_a']) & (df['Low'] > df['senkou_span_b']), 1, df['above_cloud'])
df['above_cloud'] = np.where((df['High'] < df['senkou_span_a']) & (df['High'] < df['senkou_span_b']), -1, df['above_cloud'])

# Senkou span A is above Senkou span B
df['A_above_B'] = np.where((df['senkou_span_a'] > df['senkou_span_b']),1,-1)

# Tenkan sen crossed the kinjun sen - buy
df['tenkan_kijun_cross'] = np.NaN
df['tenkan_kijun_cross'] = np.where((df['tenkan_sen'].shift(1) <= df['kijun_sen'].shift(1)) & (df['tenkan_sen'] > df['kijun_sen']), 1, df['tenkan_kijun_cross'])
df['tenkan_kijun_cross'] = np.where((df['tenkan_sen'].shift(1) >= df['kijun_sen'].shift(1)) & (df['tenkan_sen'] < df['kijun_sen']), -1, df['tenkan_kijun_cross'])

# Tenkan sen crossed the market
df['price_tenkan_cross'] = np.NaN
df['price_tenkan_cross'] = np.where((df['Open'].shift(1) <= df['tenkan_sen'].shift(1)) & (df['Open'] > df['tenkan_sen']), -1, df['price_tenkan_cross'])
df['price_tenkan_cross'] = np.where((df['Open'].shift(1) >= df['tenkan_sen'].shift(1)) & (df['Open'] < df['tenkan_sen']), 1, df['price_tenkan_cross'])


df['buy'] = np.NaN
df['buy'] = np.where((df['above_cloud'].shift(1) == 1) & (df['A_above_B'].shift(1) == 1) & ((df['price_tenkan_cross'].shift(1) == 1) | (df['tenkan_kijun_cross'].shift(1) == 1)), 1, df['buy'])
df['buy'] = np.where(df['tenkan_kijun_cross'].shift(1) == -1, 0, df['buy'])
df['buy'].ffill(inplace=True)

df['sell'] = np.NaN
df['sell'] = np.where((df['above_cloud'].shift(1) == -1) & (df['A_above_B'].shift(1) == -1) & ((df['price_tenkan_cross'].shift(1) == -1) | (df['tenkan_kijun_cross'].shift(1) == -1)), 1, df['sell'])
df['sell'] = np.where(df['tenkan_kijun_cross'].shift(1) == 1, 0, df['sell'])
df['sell'].ffill(inplace=True)

df['position'] = df['buy'] + df['sell']
df['stock_returns'] = np.log(df['Open']) - np.log(df['Open'].shift(1))
df['strategy_returns'] = df['stock_returns'] * df['position']
#df[['stock_returns', 'strategy_returns']].cumsum().plot(figsize=(15, 8))



print(df)

fig2.add_trace(go.Scatter(x=df.index, y=df['strategy_returns'], line=dict(color='royalblue', width=.7), name='Strategy Returns'))
fig2.add_trace(go.Scatter(x=df.index, y=df['stock_returns'], line=dict(color='red', width=.7), name='Stock Returns'))
fig2.show()

#fig.show()

