import requests
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import defs


# OANDA's token: 770dd2e56213d50c5b1133f3c342d38e-120bf49185606985aded5841726bbdc1
# OANDA's account number: 101-001-24825145-001


session = requests.Session()

instr_df = pd.read_pickle('instruments.pkl')
our_curr = ['EUR', 'USD', 'GBP', 'JPY', 'CHF', 'NZD', 'CAD']


def fetch_candles(pair_name, count, granularity):
    url = f'{defs.OANDA_URL}/instruments/{pair_name}/candles'
    param = dict(
        count = count,
        granulaarity = granularity,
        price = 'MBA'
    )
    response = session.get(url, params = param, headers=defs.SECURE_HEADER)
    return response.status_code, response.json()


def get_candles_df(json_response):
    prices = ['mid', 'bid', 'ask']
    ohlc = ['o', 'h', 'l', 'c']

    our_data = []
    for candle in json_response['candles']:
        if candle['complete'] == False:
            continue
        new_dict = {}
        new_dict['time'] = candle['time']
        new_dict['volume'] = candle['volume']
        
        for price in prices:
            for oh in ohlc:
                new_dict[f'{price}_{oh}'] = candle[price][oh]

        our_data.append(new_dict)
    return pd.DataFrame.from_dict(our_data)

def save_file(candles_df, pair, granularity):
    candles_df.to_pickle(f'hist_data/{pair}_{granularity}.pkl')

def create_data(pair, granularity):
    code, json_data = fetch_candles(pair, 4000, granularity)
    if code != 200:
        print(pair, 'Error')
        return
    df = get_candles_df(json_data)
    print(f'{pair} loaded {df.shape[0]} candles from {df.time.min()} to {df.time.max()}')
    save_file(df, pair, granularity)


for p1 in our_curr:
    for p2 in our_curr:
        pair = f'{p1}_{p2}'
        if pair in instr_df.name.unique():
            create_data(pair, 'H1')