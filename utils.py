import datetime as dt
from dateutil.parser import *

def get_his_data_filename(pair, granularity):
    return f'hist_data/{pair}_{granularity}.pkl'

def get_instruments_data_filename():
    return "instruments.pkl"

def time_utc():
    return dt.datetime.utcnow().replace(tzinfo=dt)

if __name__ == '__main__':
    print(get_his_data_filename("EUR_USD", "H1"))
    print(get_instruments_data_filename())