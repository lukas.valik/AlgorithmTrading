import requests
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import defs


# OANDA's token: 770dd2e56213d50c5b1133f3c342d38e-120bf49185606985aded5841726bbdc1
# OANDA's account number: 101-001-24825145-001


session = requests.Session()
instrument = 'EUR_USD'
count = 10
granularity = 'H1'

url = f'{defs.OANDA_URL}/instruments/{instrument}/candles'

param = dict(
    count = count,
    granulaarity = granularity,
    price = 'MBA'
)

response = session.get(url, params = param, headers=defs.SECURE_HEADER)

data = response.json()

prices = ['mid', 'bid', 'ask']
ohlc = ['o', 'h', 'l', 'c']

our_data = []
for candle in data['candles']:
    if candle['complete'] == False:
        continue
    new_dict = {}
    new_dict['time'] = candle['time']
    new_dict['volume'] = candle['volume']
    
    for price in prices:
        for oh in ohlc:
            new_dict[f'{price}_{oh}'] = candle[price][oh]

    our_data.append(new_dict)

candles_df = pd.DataFrame.from_dict(our_data)
print(candles_df)