import requests as re
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import defs

# OANDA's token: 770dd2e56213d50c5b1133f3c342d38e-120bf49185606985aded5841726bbdc1
# OANDA's account number: 101-001-24825145-001

session = re.Session()
url = f'{defs.OANDA_URL}/accounts/{defs.ACCOUNT_ID}/instruments'

response = session.get(url, params = None, headers=defs.SECURE_HEADER)
data = response.json()

instruments = data['instruments']

instrument_data = []
for item in instruments:
    new_ob = dict(
        name = item['name'],
        type = item['type'],
        displayName = item['displayName'],
        pipLocation = item['pipLocation'],
        marginRate = item['marginRate'],
    )
    instrument_data.append(new_ob)

instrument_df = pd.DataFrame.from_dict(instrument_data)

instrument_df.to_pickle('instruments.pkl')